package be.kdg.java2.exercise1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Person {
    private Logger logger = LoggerFactory.getLogger(Person.class);

    private Robot robot;

    @Autowired
    @Qualifier("myLoudRobot")
    public void setRobot(Robot robot) {
        logger.debug("Setting the robot to " + robot);
        this.robot = robot;
    }

    public void useRobot() {
        logger.info("Using the robot...");
        this.robot.sayHello();
    }
}
