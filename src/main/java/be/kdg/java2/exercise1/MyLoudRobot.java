package be.kdg.java2.exercise1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MyLoudRobot implements Robot{
    private Logger logger = LoggerFactory.getLogger(MyLoudRobot.class);
    @Override
    public void sayHello() {
        logger.warn("HELLO THERE I'M A ROBOT!");
    }
}
