package be.kdg.java2.exercise1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
//@Scope("prototype")
public class MyRobot implements Robot{
    private Logger logger = LoggerFactory.getLogger(MyRobot.class);

    public MyRobot() {
        logger.debug("Someone is creating a robot...!");
    }

    @Override
    public void sayHello() {
        logger.info("Hello there, I'm your robot...");
    }
}
