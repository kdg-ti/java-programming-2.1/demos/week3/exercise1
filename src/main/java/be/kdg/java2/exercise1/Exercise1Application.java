package be.kdg.java2.exercise1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
public class Exercise1Application {
    private static Logger logger = LoggerFactory.getLogger(Exercise1Application.class);

    public static void main(String[] args) {
        logger.debug("Starting the main method...");
        ApplicationContext context = SpringApplication.run(Exercise1Application.class, args);
        Robot robot1 = context.getBean("myLoudRobot", Robot.class);
        robot1.sayHello();
        // Robot robot2 = context.getBean(Robot.class);
        // robot2.sayHello();
        logger.info("Robot1:" + robot1);
        // System.out.println("Robot2:" + robot2);
    }

    //@Bean
    public Robot myRobot() {
        return new MyRobot();
    }
}
